﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceProcess;
using System.Diagnostics;
using System.IO;

namespace BoxStatus
{
    class BoxesCheck
    {
        public static void initializeCheck()
        {
            try {
                string currentDir = Path.GetDirectoryName(Program.assDir).Replace(@"file:\", "");
                string log = "";

                Parallel.ForEach(Program.MANBoxes, manBox =>
                {
                    if (manBox.isOnline = checkIsOnline(manBox.eBoxHostName))
                    {
                        log += manBox.eBoxHostName + " is ONLINE\n";
                        if (!Directory.Exists(@"\\" + manBox.eBoxHostName + @"\c$\everseen\status"))
                        {
                            try
                            {
                                Directory.CreateDirectory(@"\\" + manBox.eBoxHostName + @"\c$\everseen\status");
                                log += manBox.eBoxHostName + " status dir created successfully\n";
                            }
                            catch (Exception e) {
                                log += manBox.eBoxHostName + " " + e.Message + " when trying to create Status dir\n";
                            }

                        }
                        else {
                            log += manBox.eBoxHostName + " status dir allready exists.\n";
                        }

                        try {
                            if (File.Exists(@"\\" + manBox.eBoxHostName + @"\c$\everseen\status\statusSCO.ps1"))
                            {
                                File.Delete(@"\\" + manBox.eBoxHostName + @"\c$\everseen\status\statusSCO.ps1");
                            }
                            File.Copy(currentDir + @"\statusMAN.ps1", @"\\" + manBox.eBoxHostName + @"\c$\everseen\status\statusMAN.ps1", true);
                            log += manBox.eBoxHostName + " file created succesfully.\n";
                        }
                        catch (Exception e) {
                            log += manBox.eBoxHostName + " " + e.Message + " when trying copy file\n";
                        }

                        //RUN SCRIPT
                        Process p1 = new Process();
                        p1.StartInfo.UseShellExecute = false;
                        p1.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                        p1.StartInfo.RedirectStandardOutput = true;
                        p1.StartInfo.RedirectStandardError = true;
                        p1.StartInfo.RedirectStandardInput = true;
                        p1.StartInfo.FileName = currentDir + @"\psexec.exe";

                        p1.StartInfo.Arguments = "-accepteula \\\\" + manBox.eBoxHostName + " -s -d cmd.exe /c \"powershell C:\\everseen\\status\\statusMAN.ps1\"";

                        p1.Start();
                        p1.WaitForExit();
                        string stdout = p1.StandardOutput.ReadToEnd();
                        log += stdout + "\n";

                    }
                    else {
                        log += manBox.eBoxHostName + " is OFFLINE\n";
                    }
                });

                File.WriteAllText(currentDir + @"\logMAN.log", log);
                log = "";

                Parallel.ForEach(Program.SCOBoxes, scoBox =>
                {
                    if (scoBox.isOnline = checkIsOnline(scoBox.eBoxHostName))
                    {
                        log += scoBox.eBoxHostName + " is ONLINE\n";
                        if (!Directory.Exists(@"\\" + scoBox.eBoxHostName + @"\c$\everseen\status"))
                        {
                            try
                            {
                                Directory.CreateDirectory(@"\\" + scoBox.eBoxHostName + @"\c$\everseen\status");
                                log += scoBox.eBoxHostName + " status dir created successfully\n";
                            }
                            catch (Exception e)
                            {
                                log += scoBox.eBoxHostName + " " + e.Message + " when trying to create Status dir\n";
                            }

                        }
                        else
                        {
                            log += scoBox.eBoxHostName + " status dir allready exists.\n";
                        }

                        try
                        {
                            if(File.Exists(@"\\" + scoBox.eBoxHostName + @"\c$\everseen\status\statusMAN.ps1")) {
                                File.Delete(@"\\" + scoBox.eBoxHostName + @"\c$\everseen\status\statusMAN.ps1");
                            }
                            File.Copy(currentDir + @"\statusSCO.ps1", @"\\" + scoBox.eBoxHostName + @"\c$\everseen\status\statusSCO.ps1", true);
                            log += scoBox.eBoxHostName + " file created succesfully.\n";
                        }
                        catch (Exception e)
                        {
                            log += scoBox.eBoxHostName + " " + e.Message + " when trying copy file\n";
                        }

                        //RUN SCRIPT
                        //RUN SCRIPT
                        Process p1 = new Process();
                        p1.StartInfo.UseShellExecute = false;
                        p1.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                        p1.StartInfo.RedirectStandardOutput = true;
                        p1.StartInfo.RedirectStandardError = true;
                        p1.StartInfo.RedirectStandardInput = true;
                        p1.StartInfo.FileName = currentDir + @"\psexec.exe";

                        p1.StartInfo.Arguments = "-accepteula \\\\" + scoBox.eBoxHostName + " -s -d cmd.exe /c \"powershell C:\\everseen\\status\\statusSCO.ps1\"";

                        p1.Start();

                        p1.WaitForExit();

                    }
                    else
                    {
                        log += scoBox.eBoxHostName + " is OFFLINE\n";
                    }
                });

                File.WriteAllText(currentDir + @"\logSCO.log", log);
            }
            
            catch (Exception e) {
                Console.WriteLine(e.Message);
            }
        }
        public static bool checkIsOnline(string eBoxHostName)
        {
            try
            {
                if (Utilities.ping(eBoxHostName) && Utilities.access(eBoxHostName))
                {
                    Console.WriteLine(eBoxHostName + " is ONLINE");
                    return true;
                }
                else
                {
                    Console.WriteLine(eBoxHostName + " is OFFLINE");
                    return false;
                }
            }
            catch(Exception e)
            {
                Console.WriteLine(eBoxHostName + e.Message);
                return false;
            }
            
            
        }

        private static bool checkRestartTask(string eBoxHostName)
        {
            try
            {
                if (Utilities.restartTask(eBoxHostName))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch(Exception e)
            {
                Console.WriteLine(eBoxHostName + e.Message);
                return false;
            }
           
        }

        private static bool isRunning(string eBoxHostName, string app)
        {
            try
            {
                Process[] proc = System.Diagnostics.Process.GetProcessesByName(app, eBoxHostName);
                if (proc.Length > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch(Exception e)
            {
                Console.WriteLine(eBoxHostName + e.Message);
                return false;
            }
        }

        private static string appSize(string eBoxHostName, string app)
        {
            try
            {
                string appExe = "";

                if (app == "edvr")
                {
                    appExe = @"\\" + eBoxHostName + @"\c$\everseen\" + app + @"\bin\MediaCapture.exe";
                    return new System.IO.FileInfo(appExe).Length.ToString();
                }
                if (app == "databasesynchronizer")
                {
                    appExe = @"\\" + eBoxHostName + @"\c$\everseen\dbsynchronizer\bin\databasesynchronizer.exe";
                    return new System.IO.FileInfo(appExe).Length.ToString();
                }
                if (app == "selfservice")
                {
                    appExe = @"\\" + eBoxHostName + @"\c$\everseen\selfservice\bin\VASelfService.exe";
                    return new System.IO.FileInfo(appExe).Length.ToString();
                }

                appExe = @"\\" + eBoxHostName + @"\c$\everseen\" + app + @"\bin\" + app + ".exe";
                return new System.IO.FileInfo(appExe).Length.ToString();
            }
            catch(Exception e)
            {
                Console.WriteLine(eBoxHostName + e.Message);
                return e.Message;
            }
        }

        private static bool checkCrawlerPy(string eBoxHostName)
        {
            try
            {
                if (File.Exists(@"\\" + eBoxHostName + @"\c$\everseen\managementscripts\bin\transactionmonitor.py"))
                {
                    return true;
                }
                return false;
            }
            catch(Exception e)
            {
                return false;
            }
           
        }
    }
}
