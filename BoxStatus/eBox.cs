﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoxStatus
{
    public class eBox
    {
        public string eBoxHostName { get; set; }
        public bool isOnline { get; set; }
        public bool isRegistered { get; set; }
        public bool hasRestartTask { get; set; }
        public bool overseerRunning { get; set; }
        public string overseerSize { get; set; }
        public bool updaterRunning { get; set; }
        public string updaterSize { get; set; }
        public bool diskmanagerRunning { get; set; }
        public string diskmanagerSize { get; set; }
        public string shcfgSize { get; set; }
        public bool dbsyncRunning { get; set; }
        public string dbsyncSize { get; set; }
        public bool mysqlRunning { get; set; }
        public bool overseerCfgGood { get; set; }
        public bool shcfgConfigIsGood { get; set; }


    }
}
