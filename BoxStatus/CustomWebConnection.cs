﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;


using Newtonsoft;

namespace BoxStatus
{
     public class CustomWebConnection
    {
        public System.Net.HttpWebRequest request;
        private string cookie;
        
        private CookieContainer GetCoockies()
        {
            System.Net.HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create("https://csph.ozarkservices.com/login/login");
            WebProxy wp = new WebProxy("sysproxy.wal-mart.com:8080");
            httpWebRequest.Proxy = wp;
            string postParams = string.Format("UserName={0}&UserPassword={1}", "dan.cioara", "!234Everseen0");
            byte[] postDataBytes = new ASCIIEncoding().GetBytes(postParams);

            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            httpWebRequest.Method = WebRequestMethods.Http.Post;

            httpWebRequest.ContentLength = postParams.Length;
            httpWebRequest.AllowAutoRedirect = false;
            httpWebRequest.ContentType = "application/x-www-form-urlencoded; charset=UTF-8";


            using (var stream = new StreamWriter(httpWebRequest.GetRequestStream()))
            {

                stream.Write(postParams);
                stream.Close();
            }
            
            var httpWebResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            this.cookie = Regex.Match(httpWebResponse.Headers["Set-Cookie"], @"PHPSESSID=([a-z0-9]*)").ToString();

            using (var reader = new StreamReader(httpWebResponse.GetResponseStream()))
            {
                string objText = reader.ReadToEnd();
            }

            return httpWebRequest.CookieContainer;
        }

         public CustomWebConnection(string path)
        {
            this.request = (HttpWebRequest)WebRequest.Create(path);
            WebProxy wp = new WebProxy("sysproxy.wal-mart.com:8080");
            this.request.Proxy = wp;
            this.request.CookieContainer = GetCoockies();
            this.request.Headers["Cookie"] = this.cookie;
            this.request.AllowAutoRedirect = false;
        }
        
        public HttpWebResponse GetResponse()
        {
            try
            {
                var ret = (HttpWebResponse)this.request.GetResponse();
                return ret;
            }
            catch { return null; }
        }

        public void AddContent(string payload)
        {

            this.request.Method = "POST";
            this.request.ContentLength = payload.Length;
            this.request.Headers["X-Requested-With"] = "XMLHttpRequest";
            this.request.ContentType = "application/x-www-form-urlencoded; charset=utf-8";

            using (var stream = new StreamWriter(this.request.GetRequestStream()))
            {
                stream.Write(payload);
                stream.Close();
            }
        }
        public void checkMANRegistered(List<MANBox> eBoxList)
        {

            var response = (HttpWebResponse)this.request.GetResponse();
            if (response == null)
            {
                return;
            }

            string replay;

            using (var reader = new StreamReader(response.GetResponseStream()))
            {
                replay = reader.ReadToEnd();
            }

            CsphEBoxList csphBoxes = Newtonsoft.Json.JsonConvert.DeserializeObject<CsphEBoxList>(replay);

            foreach (eBox ebox in eBoxList)
            {
                
                    string eboxName = Regex.Match(ebox.eBoxHostName.ToLower(), @"^(.*?)\.s").Groups[1].ToString();
                    string eboxProdStore = "WM-" + Regex.Match(ebox.eBoxHostName.ToLower(), @"\.s(.*?)\.us").Groups[1].ToString();
                    foreach (CspheBox csphBox in csphBoxes.CsphEboxes)
                    {
                        if (csphBox.Name.Contains(eboxName) && eboxProdStore == csphBox.StoreProductionName)
                        {
                            ebox.isRegistered = true;
                            break;
                        }
                        else
                        {
                            ebox.isRegistered = false;
                            continue;
                        }
                    }
                
            }
        }
        public void checkSCORegistered(List<SCOBox> eBoxList)
        {

            var response = (HttpWebResponse)this.request.GetResponse();
            if (response == null)
            {
                return;
            }

            string replay;

            using (var reader = new StreamReader(response.GetResponseStream()))
            {
                replay = reader.ReadToEnd();
            }

            CsphEBoxList csphBoxes = Newtonsoft.Json.JsonConvert.DeserializeObject<CsphEBoxList>(replay);

            foreach (eBox ebox in eBoxList)
            {

                string eboxName = Regex.Match(ebox.eBoxHostName.ToLower(), @"^(.*?)\.s").Groups[1].ToString();
                string eboxProdStore = "WM-" + Regex.Match(ebox.eBoxHostName.ToLower(), @"\.s(.*?)\.us").Groups[1].ToString();
                foreach (CspheBox csphBox in csphBoxes.CsphEboxes)
                {
                    if (csphBox.Name.Contains(eboxName) && eboxProdStore == csphBox.StoreProductionName)
                    {
                        ebox.isRegistered = true;
                        break;
                    }
                    else
                    {
                        ebox.isRegistered = false;
                        continue;
                    }
                }

            }
        }
    }
}
