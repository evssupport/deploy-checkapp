﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoxStatus
{
    public class SCOBox : eBox
    {
        public SCOBox(string boxHostName)
        {
            eBoxHostName = boxHostName;
        }
        public bool amqRunning { get; set; }
        public string amqSize { get; set; }
        public bool selfserviceRunning { get; set; }
        public string selfserviceSize { get; set; }

    }
}
