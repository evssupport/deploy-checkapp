﻿
function Get-Process-Info{
    param($Process)
    return Get-Process $Process -ErrorAction SilentlyContinue
}

function Get-File-Size{
    param($App)
    $file = "c:\everseen\$App\bin\$App.exe"
    return((Get-Item $file).Length)
}

function Get-File-Size-ss{
    param($App)
    $file = "c:\everseen\selfservice\bin\$App.exe"
    return((Get-Item $file).Length)
}

function Get-File-Size-db{
    param($App)
    $file = "c:\everseen\dbsynchronizer\bin\$App.exe"
    return((Get-Item $file).Length)
}

$processes = "overseer","updater","diskmanager","shopconfig","databasesynchronizer","mysqld","FALSE","FALSE","activemq-loader","vaselfservice"
Clear-Content c:\everseen\status\BoxStatus.csv



if(Get-ScheduledTask "OverseerRestart"){
    Add-Content c:\everseen\status\BoxStatus.csv "TRUE," -NoNewline
    Get-ScheduledTask "OverseerRestart" |
        ForEach-Object { 
            if(!($_.Actions.execute -eq "taskkill")){
                $action = New-ScheduledTaskAction -Execute "taskkill" -Argument "/im Overseer.exe /f /t"
                Set-ScheduledTask "OverseerRestart" -Action $action
            }
            if(!($_.Actions.Arguments -eq "/im Overseer.exe /f /t")){
                $action = New-ScheduledTaskAction -Execute "taskkill" -Argument "/im Overseer.exe /f /t"
                Set-ScheduledTask "OverseerRestart" -Action $action
            } 
        }
}
else{
    SchTasks.exe /create /RU SYSTEM /SC DAILY /TN "OverseerRestart" /TR "taskkill /im Overseer.exe /f /t" /ST 00:00 /F
    Add-Content c:\everseen\status\BoxStatus.csv "TaskCreated," -NoNewline
}

foreach ($process in $processes){
    if($process -eq "mysqld"){
        if(Get-Process-Info -Process $process){
            $processR = "TRUE"
        }
        else{
            $processR = "FALSE"
        }
        Add-Content c:\everseen\status\BoxStatus.csv "$processR," -NoNewline
     }
     elseif($process -eq "shopconfig"){
        $processSize =  (Get-File-Size -App $process)
        if($processSize -eq 0){
            $processSize = ""
            }
                Add-Content c:\everseen\status\BoxStatus.csv "$processSize," -NoNewline
        }
        elseif($process -eq "vaselfservice"){
            if(Get-Process-Info -Process $process){
                $processR = "TRUE"
            }
            else{
                $processR = "FALSE"
            }

            $processSize =  (Get-File-Size-ss -App $process)
            if($processSize -eq 0){
                $processSize = ""
            }   
            Add-Content c:\everseen\status\BoxStatus.csv "$processR,$processSize," -NoNewline 
        }
        elseif($process -eq "FALSE"){
            Add-Content c:\everseen\status\BoxStatus.csv "FALSE," -NoNewline 
        }
         elseif($process -eq "databasesynchronizer"){
            if(Get-Process-Info -Process $process){
                $processR = "TRUE"
            }
            else{
                $processR = "FALSE"
            }

            $processSize =  (Get-File-Size-db -App $process)
            if($processSize -eq 0){
                $processSize = ""
            }
            Add-Content c:\everseen\status\BoxStatus.csv "$processR,$processSize," -NoNewline
            
        }
        else{
            if(Get-Process-Info -Process $process){
                $processR = "TRUE"
            }
            else{
                $processR = "FALSE"
            }

            $processSize =  (Get-File-Size -App $process)
            if($processSize -eq 0){
                $processSize = ""
            }
            Add-Content c:\everseen\status\BoxStatus.csv "$processR,$processSize," -NoNewline
        }
}