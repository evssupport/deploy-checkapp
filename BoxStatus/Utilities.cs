﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;

namespace BoxStatus
{
    class Utilities
    {
        public static bool ping(string eBoxHostName)
        {
            Ping pinger = new Ping();
            try
            {
                string data = "a";
                byte[] buffer = Encoding.ASCII.GetBytes(data);
                PingReply reply = pinger.Send(eBoxHostName, 1000, buffer);
                if (reply.Status == IPStatus.Success)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (PingException)
            {
                return false;
            }          
        }

        public static bool access(string eBoxHostName)
        {
            if(System.IO.Directory.Exists(@"\\" + eBoxHostName + @"\c$\everseen"))
            {
                return true;
            }
            else
            {
                return false;
            }
            
        }

        public static bool restartTask(string eBoxHostName)
        {
            Process p = new Process();
            ProcessStartInfo pinfo = new ProcessStartInfo();
            pinfo.FileName = "SchTasks.exe";
            pinfo.WindowStyle = ProcessWindowStyle.Hidden;
            pinfo.Arguments = "/query /s " + eBoxHostName + " /TN \"OverSeerRestart\"";
            pinfo.UseShellExecute = false;

            p.StartInfo = pinfo;
            p.Start();
            p.WaitForExit();
            int exitcode = p.ExitCode;
            
            if(exitcode != 0)
            {
                return false;
            }

            return true;

        }
    }
}
