﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoxStatus
{
    class CsphEBoxList
    {
        [JsonProperty("eboxes")]
        public List<CspheBox> CsphEboxes { get; set; }

        public CsphEBoxList()
        {
            this.CsphEboxes = new List<CspheBox>();
        }

    }

    class CspheBox
    {
        [JsonProperty("Id")]
        public string Id { get; set; }
        [JsonProperty("StoreProductionName")]
        public string StoreProductionName { get; set; }
        [JsonProperty("Name")]
        public string Name { get; set; }
        [JsonProperty("VSN")]
        public string VSN { get; set; }
        [JsonProperty("BoxNumber")]
        public string BoxNumber { get; set; }
        [JsonProperty("UserId")]
        public string UserId { get; set; }
        [JsonProperty("MAC")]
        public string MAC { get; set; }
        [JsonProperty("Key")]
        public string Key { get; set; }
        [JsonProperty("EncryptedKey")]
        public string EncryptedKey { get; set; }
        [JsonProperty("TimeStamp")]
        public string TimeStamp { get; set; }
        [JsonProperty("Timezone")]
        public string Timezone { get; set; }
        [JsonProperty("ELicense")]
        public string ELicense { get; set; }
        [JsonProperty("HddId")]
        public string HddId { get; set; }
        [JsonProperty("EboxId")]
        public string EboxId { get; set; }
        [JsonProperty("Entries")]
        public string Entries { get; set; }
        [JsonProperty("Warnings")]
        public string Warnings { get; set; }
        [JsonProperty("Errors")]
        public string Errors { get; set; }
        [JsonProperty("LastContact")]
        public string LastContact { get; set; }
        [JsonProperty("LastLog")]
        public string LastLog { get; set; }
    }
}
