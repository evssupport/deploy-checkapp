﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoxStatus
{
    class CreateOutput
    {
        public static void createCSV()
        {
            string currentDir = Path.GetDirectoryName(Program.assDir).Replace(@"file:\", "");

            if (File.Exists(currentDir + @"\MANBoxes.csv"))
            {
                try
                {
                    File.Delete(currentDir + @"\MANBoxes.csv");
                }
                catch (Exception e)
                {
                    //no sure
                }
            }

            if (File.Exists(currentDir + @"\SCOBoxes.csv"))
            {
                try
                {
                    File.Delete(currentDir + @"\SCOBoxes.csv");
                }
                catch(Exception e)
                {
                    //not sure
                }
            }


            StringBuilder csvContent = new StringBuilder();

            //CREATING MAN CSV
            csvContent.Clear();
            string csvPath = currentDir + @"\MANBoxes.csv";

            csvContent.AppendLine("Box,Online,HasRRTask,OverseerR,OverseerS,UpdaterR,UpdaterS,DiskManR,DsiskManS,ShcfgAppS,EdvrR,EdvrS,DbsyncR,DbsyncS,MysqlR,OverseerCfg,ShcfgCfg,CrawlerR,CrawlerS,VideoMonR,VideoMonS,PLoadR,PLoadS,CrawlerPy");
            File.AppendAllText(csvPath, csvContent.ToString());
            csvContent.Clear();

            Parallel.ForEach(Program.MANBoxes, manBox =>
            {
                if (manBox.isOnline)
                {
                    try {
                        string boxContent = manBox.eBoxHostName + "," + manBox.isOnline + "," + File.ReadAllText(@"\\" + manBox.eBoxHostName + @"\c$\everseen\status\BoxStatus.csv");
                        csvContent.AppendLine(boxContent);
                    }
                    catch(Exception e) {
                        csvContent.AppendLine(manBox.eBoxHostName + ",ERROR");
                    }
                }
                else {
                    csvContent.AppendLine(manBox.eBoxHostName + "," + manBox.isOnline);
                }
            });
            File.AppendAllText(csvPath, csvContent.ToString());


            //CREATING SCO CSV
            csvContent.Clear();
            csvPath = currentDir + @"\SCOBoxes.csv";

            csvContent.AppendLine("Box,Online,HasRRTask,OverseerR,OverseerS,UpdaterR,UpdaterS,DiskManR,DsiskManS,ShcfgAppS,DbsyncR,DbsyncS,MysqlR,OverseerCfg,ShcfgCfg,AmqR,AmqS,SsR,SsS");
            File.AppendAllText(csvPath, csvContent.ToString());
            csvContent.Clear();

            Parallel.ForEach(Program.SCOBoxes, scoBox =>
            {
                if (scoBox.isOnline)
                {
                    try
                    {
                        string boxContent = scoBox.eBoxHostName + "," + scoBox.isOnline + "," + File.ReadAllText(@"\\" + scoBox.eBoxHostName + @"\c$\everseen\status\BoxStatus.csv");
                        csvContent.AppendLine(boxContent);
                    }
                    catch (Exception e)
                    {
                        csvContent.AppendLine(scoBox.eBoxHostName + ",ERROR");
                    }
                }
                else
                {
                    csvContent.AppendLine(scoBox.eBoxHostName + "," + scoBox.isOnline);
                }
            });
            File.AppendAllText(csvPath, csvContent.ToString());
        }
    }
}
