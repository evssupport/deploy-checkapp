﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoxStatus
{
    public class MANBox : eBox
    {
        public MANBox(string boxHostName)
        {
            eBoxHostName = boxHostName;
        }
        public string edvrSize { get; set; }
        public bool edvrRunning { get; set; }
        public bool crawlerRunning { get; set; }
        public string crawlerSize { get; set; }
        public bool videomonitorRunning { get; set; }
        public string videomonitorSize { get; set; }
        public bool posloaderRunning { get; set; }
        public string posloaderSize { get; set; }
        public bool crawlerPy { get; set; }

    }
}
