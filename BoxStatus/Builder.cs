﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace BoxStatus
{
    class Builder
    {
        public static void buildList()
        {
            string[] storesDirs;
            string downloadedConfiguration = Path.GetDirectoryName(Program.assDir) + @"\DownloadedConfiguration";
            storesDirs = Directory.GetDirectories(downloadedConfiguration);

            Parallel.ForEach(storesDirs, storeDir =>
            {
                string store = "s" + storeDir.Substring(storeDir.Length - 5);
                string[] boxesDirs = Directory.GetDirectories(storeDir);
                foreach (string boxDir in boxesDirs)
                {
                    string boxDirName = boxDir.Substring(boxDir.Length - 10);
                    string boxType = boxDir.Substring(boxDir.Length - 3);
                    string boxHostName = boxDirName.Substring(0, boxDirName.Length - 4) + "." + store + ".us";

                    if (boxType == "MAN")
                    {
                        Program.MANBoxes.Add(new MANBox(boxHostName));
                    }
                    else
                    {
                        Program.SCOBoxes.Add(new SCOBox(boxHostName));
                    }

                }
            });

        }

        public static void buildListFromCSV()
        {
            string currentDir = Path.GetDirectoryName(Program.assDir).Replace(@"file:\", "");
            Console.WriteLine(currentDir);
            var client = new WebClient { Credentials = new NetworkCredential("wuser", "BylOpwgT8Vb3t%") };
            WebProxy wp = new WebProxy("sysproxy.wal-mart.com:8080");
            client.Proxy = wp;

            client.DownloadFile("https://csph.ozarkservices.com/webservices/eBoxes/AllMANBoxes.csv", currentDir + @"\AllMANBoxes.csv");
            client.DownloadFile("https://csph.ozarkservices.com/webservices/eBoxes/AllSCOBoxes.csv", currentDir + @"\AllSCOBoxes.csv");

            string[] boxes = File.ReadAllLines(currentDir + @"\AllMANBoxes.csv");

            foreach (string box in boxes)
            {
                Program.MANBoxes.Add(new MANBox(box));
            }

            boxes = File.ReadAllLines(currentDir + @"\AllSCOBoxes.csv");

            foreach (string box in boxes)
            {
                Program.SCOBoxes.Add(new SCOBox(box));
            }
        }
    }
}
